export const laptops = [
    {
        // asus rog 
        "name": "ASUS ROG Zephyrus Duo GX550LXS 15.6",
        "image": "./img/ASUS ROG Zephyrus Duo.png", 
        "price": 45990, 
        "feature": "Microsoft, Windows 10 Home, GeForce RTX 2080 Super, Core i7-10875H, 32 GB RAM, 1 TB SSD, NumberPad",
        "description": "Zephyrus Duo 15 lets you expand your horizons for creativity and gaming, and comes with a secondary ROG ScreenPad Plus screen that takes the use of Windows 10 to new heights."
    },

    {
        // asus zenbook pro 
        "name": "ASUS ZenBook Pro Duo UX581LV 15.6",
        "image": "./img/ASUS ZenBook Pro Duo.png", 
        "price": 38990, 
        "feature": "Microsoft, Windows 10 Pro, GeForce RTX2060, Core i9-10980HK, 32GB RAM, 1TB SSD, ScreenPad Plus",
        "description": "With a laptop like Zenbook Pro Duo, the term desktop replacement takes on a whole new meaning. For the first time ever, you have two large usable screens that give your workflows new dimensions."
    },

    {
        // apple air 13 
        "name": "Macbook Air 13",
        "image": "./img/macbook air13.png", 
        "price": 15290, 
        "feature": "Apple, iOS, Intel 10th-gen Dual-Core i3 1.1GHz, 16GB RAM, 256GB SSD, Iris Plus Graphics",
        "description": "The latest MacBook Air has a stunning Retina display with True Tone technology, a backlit Magic Keyboard, Touch ID, a 10th generation Intel Core processor and a battery that lasts all day.¹ All in one thin and light, perfect laptop design."
    },

    {
        // apple pro 13
        "name": "Macbook Pro 13",
        "image": "./img/macbook pro13.png", 
        "price": 19290, 
        "feature": "Apple, iOS, Intel 8th gen. Quad-Core i5 1.4GHz, 16GB RAM, 256GB SSD, Intel Iris Plus Graphic",
        "description": "MacBook Pro sets a new standard for performance and functionality in laptops. No matter where your ideas take you, everything goes faster than ever with top-class processors and memory, advanced graphics, lightning-fast storage technology and much more - in a compact package of one and a half kilos."
    },


    {
        // apple pro 16
        "name": "Macbook Pro 16",
        "image": "./img/macbook pro16.png", 
        "price": 40240, 
        "feature": "Apple, iOS, Intel 8-core i9 2.3GHz, 32GB RAM, 1TB SSD, Radeon Pro 5500M 8GB", 
        "description": "The new MacBook Pro is for those who want to push boundaries and change the world. It has power and performance that surpasses any laptop Apple has made in the past. With a stunning 16-inch Retina display, lightning-fast processors, next-generation graphics, the largest battery capacity in a MacBook Pro, a new Magic Keyboard and huge storage space, this is the ultimate laptop for ultimate users."
    },


    {
        // msi gs66
        "name": "MSI GS66 Stealth 15.6",
        "image": "./img/MSI GS66.png", 
        "price": 20990,
        "feature": "Microsoft, Windows 10 Home, GeForce RTX 2060, Core i7-10750H, 16 GB RAM, 1 TB SSD", 
        "description": "The matte black and low-profile design of the all-new GS66 Stealth keeps true to the Stealth moniker, allowing you to blend in within any situation. The reinforced black metal chassis makes it more rigid perfect for daily use. The GS66 Stealth is not only a powerful portable gaming laptop but it’s an essential tool that delivers an understated confidence within the gaming and professional world."

    },


    {
        // msi p75
        "name": "MSI P75 Creator 17.3",
        "image": "./img/msip75.png", 
        "price": 41990, 
        "feature": "Microsoft, Windows 10 Pro, GeForce RTX2080, Core i9-9880H, 32GB RAM, 2 TB SSD", 
        "description": "The world's first laptop for creators with a 9th generation Intel® Core i9 processor makes creativity run faster than you could ever have dreamed. The laptop is designed for multitasking and processing at a professional level, and gives you more time to develop your next big idea."

    },

    {
        // razorblade 15 
        "name": "Razer Blade 15 Advanced 15.6",
        "image": "./img/razorblade 15.png", 
        "price": 41490, 
        "feature": "Microsoft, Windows 10 Pro, GeForce RTX 2080 Super, Core i7-10875H, 16 GB RAM, 1 TB SSD", 
        "description": "The new Razer Blade 15 is more powerful, thanks to the new 10th generation Intel® Core i7 processor with eight cores, faster graphics with NVIDIA® GeForce RTX 2080 Super ™, and a visually stunning OLED 4K touch screen that is razor sharp (Retina). All this performance is packed in a durable, precision-cut aluminum cabinet that is more compact than ever - only 1.78 cm."
    }
    
]


export function getAllLaptops(){
    return laptops; 
}