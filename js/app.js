// import laptops.js to app.js
import { getAllLaptops } from "/js/laptops.js";

export default class App {
  constructor() {
    this.laptops = getAllLaptops(); 
    this.products = [];
    this.selected = null;

    this.bank = 0;
    this.loan = 0;
    this.hasLoan = false;
    // money earned after each work
    this.earnedMoney = 0;

    // product selection
    this.elLaptopSelector = document.getElementById("laptops");

    // create dom buttons for loan, bank, work and buy
    this.elBtnLoan = document.getElementById("btnLoan");
    this.elBtnBank = document.getElementById("btnBank");
    this.elBtnWork = document.getElementById("btnWork");
    this.elBtnBuy = document.getElementById("btnBuy");

    // output 
    this.elBalanceatBank = document.getElementById("balance"); // balance at the Bank
    this.elTransfertoBank = document.getElementById("pay"); // transfer to the bank

    // create dom for image, name, description, and price 
    this.elImg = document.getElementById("image");
    this.elName = document.getElementById("name");
    this.elFeature = document.getElementById("feature"); 
    this.elDescription = document.getElementById("description");
    this.elPrice = document.getElementById("price");
    

    // call on the functions
    this.Populate();
    this.RenderTop();

    // select the desired product
    this.elLaptopSelector.onchange = (e) => {
        this.OnSelect(e.target.value)
    }

    // Work Button
    // click on work will get you money 100 NOK each time
    this.elBtnWork.onclick = _ => {
        this.earnedMoney += 10000; // 100 for the assignment and feel free to change ;) i just changed to 10000 to test faster 
        this.RenderTop();
    }
    
    // Bank bottom
    // click on Bank will transfer earned money to the Bank
    this.elBtnBank.onclick = _ => {
        this.bank += this.earnedMoney;
        this.earnedMoney = 0;
        this.RenderTop();
    }
    
    // Get a Loan button 
  
    this.elBtnLoan.onclick = _ => {
        //  you cannot get more than one bank loan before buying a computer
        if (this.hasLoan) {
          alert("You already have an existing loan that's active.");
          return;
        }
        let maxAmount = this.bank * 2;
         
         // only number is allowed to enter 
         let amount = prompt("Please enter the amount you wish to loan?")
         amount = parseInt(amount);
        
        // return the invalid format if the entered number is negtaive
        if (isNaN(amount) || amount < 0)
        {
          alert("The amount you entered is invalid.");
          return;
        }
         //you cannot get a loan more than double of your bank balance 

        if (amount > maxAmount)
        {
          alert("The amount you've requested is over the maximum amount we can loan out to you.");
          return;
        }

        this.loan += amount;
        this.bank += this.loan;
        this.hasLoan = true;

        // to update the balance at the bank 
        this.RenderTop();
    }


    // Buy a laptop button 
    // constraints for this button
    /*
    
  


   
    */
    this.elBtnBuy.onclick = _ => {
      //check if you have sufficient money in your bank's balance to buy a laptop or not
       /*when you do have enough money to buy the  selected laptop, deduce from the bank's balance
    and receive a message that you are now the 
    owner of the new laptop*/
      if(this.bank >= this.selected.price)
         alert(`Congratulations, you just bought a ${this.selected.name} with ${this.selected.price}NOK and it will be shipped within 1-2 days to your location`);
      else
      //if you do not have, show a message that you cannot afford the laptop
         alert("you don't have money to buy this laptop yet. please try one more time");

      this.bank -= this.selected.price; 
      this.RenderTop();
    }


  }

  OnSelect(index) {
     // get the list of the products by their index
      let laptop = this.products[index];
      this.selected = laptop; // set the selected to laptop
      // call on Render functionfddddd
      this.Render();
  }

  Populate() {
      this.products = getAllLaptops(); // get all laptops

      this.products.forEach((laptop, index) => {
          let item = document.createElement("option");
          item.innerText = laptop.name; // get the name of the laptop
          item.setAttribute("value", index); // set the id of the selected item

          this.elLaptopSelector.appendChild(item);
      });
  }

  Render() {
      // remove old image(s) when the new image is selected
      while (this.elImg.firstChild) {
        this.elImg.removeChild(this.elImg.firstChild);
    }

    let laptop = this.selected;
    // Load new image
      
    let img = new Image();
    
    img.src = laptop.image;
    img.onload = _ => {
        this.elImg.appendChild(img);
        
    }

    this.elName.innerText = laptop.name; 
    this.elFeature.innerText = laptop.feature; 
    this.elDescription.innerText = laptop.description;
    this.elPrice.innerText = `NOK ${laptop.price},- `;

    // Update other elements
    this.RenderTop();
  }

  // update 
  RenderTop() {
    this.elBalanceatBank.value = this.bank;
    this.elTransfertoBank.value = this.earnedMoney;
  }
}


// creating event listeners for the assignment requirements
const app = new App()